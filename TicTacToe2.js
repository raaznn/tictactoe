
var index1;
var board = ['','','','','','','','',''];

function refresh() {
    
    for(var i =0 ; i<9; i++) {
    var buttons = document.getElementById(i);
    buttons.value = board[i];
    }

}

function ai_move() {
    board[index1] = 'X';
    refresh();
}

function win () {
    var w = document.getElementById("win");
    if(cpu_won()) 
        w.innerHTML='You Lose';
    else if(user_won()) 
        w.innerHTML='You Won';
    else if(isFull()) 
        w.innerHTML='Draw';
}




function isFull()// Board is full
{
    for(var i =0;i<9;i++)
    {
        if(board[i]!='X')
        {
            if(board[i]!='O')
               {
                 return 0;
               }
        }
    }
return 1;
}

function user_won()//Checks whether user has won
{
    for(var i=0;i<9;i+=3)
    {
        if((board[i]==board[i+1])&&(board[i+1]==board[i+2])&&(board[i]=='O'))
            return 1;
    }
    for(var i=0;i<3;i++)
    {
        if((board[i]==board[i+3])&&(board[i+3]==board[i+6])&&(board[i]=='O'))
            return 1;
    }
    if((board[0]==board[4])&&(board[4]==board[8])&&(board[0]=='O'))
    {
        return 1;
    }
    if((board[2]==board[4])&&(board[4]==board[6])&&(board[2]=='O'))
    {
        return 1;
    }
    return 0;
}

function cpu_won()// Checks whether CPU has won
{
    for(var i=0;i<9;i+=3)
    {
        if((board[i]==board[i+1])&&(board[i+1]==board[i+2])&&(board[i]=='X'))
            return 1;
    }
    for(var i=0;i<3;i++)
    {
        if((board[i]==board[i+3])&&(board[i+3]==board[i+6])&&(board[i]=='X'))
            return 1;
    }
    if((board[0]==board[4])&&(board[4]==board[8])&&(board[0]=='X'))
    {
        return 1;
    }
    if((board[2]==board[4])&&(board[4]==board[6])&&(board[2]=='X'))
    {
        return 1;
    }
    return 0;
}


function minimax(flag)// The minimax function
{

    var max_val=-1000,min_val=1000;
    var i,j,value = 1;
    if(cpu_won() == 1)
        {return 10;}
    else if(user_won() == 1)
        {return -10;}
    else if(isFull()== 1)
        {return 0;}
    var score = [1,1,1,1,1,1,1,1,1];//if score[i]=1 then it is empty

        for(i=0;i<9;i++)
            {
                 if(board[i] == '')
                {
                    if(min_val>max_val) // reverse of pruning condition.....
                  {
                      if(flag == true)
                   {
                     board[i] = 'X';
                     value = minimax(false);
                   }
                    else
                    {
                      board[i] = 'O';
                      value = minimax(true);
                    }
                  board[i] = '';
                  score[i] = value;
                 }
               }
            }

        if(flag == true)
        {
                 max_val = -1000;
                 for(j=0;j<9;j++)
                {
                    if(score[j] > max_val && score[j] != 1)
                    {
                        max_val = score[j];
                        index1 = j;
                    }
                }
                return max_val;
        }
        if(flag == false)
        {
                min_val = 1000;
                for(j=0;j<9;j++)
                {
                    if(score[j] < min_val && score[j] != 1)
                    {
                        min_val = score[j];
                        index1 = j;
                    }
                }
            return min_val;
        }
}

